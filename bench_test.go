package bench

import (
	"testing"
)

var (
	testMap = map[string]bool{
		"test1": true,
		"test2": true,
		"test3": true,
		"test4": true,
		"test5": true,
		"test6": true,
	}

	testSlice = []string{
		"test1",
		"test2",
		"test3",
		"test4",
		"test5",
		"test6",
	}
)

func BenchmarkSearchSlices(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SearchSlice(testSlice, "test1")
	}
}

func BenchmarkSearchMapSlices(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SearchMap(testMap, "test1")
	}
}
