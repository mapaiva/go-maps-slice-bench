# Go Maps/Slices Benchmark #

This is a tiny benchmarking for comparing search performace against `maps` and `slices` in Golang.

## Run the test

```sh
 go test -bench=. -benchtime=20s
```

