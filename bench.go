package bench

import "fmt"

func main() {
	fmt.Println("vim-go")
}

func SearchSlice(s []string, v string) bool {
	for _, i := range s {
		if i == v {
			return true
		}
	}

	return false
}

func SearchMap(m map[string]bool, v string) bool {
	return m[v]
}
